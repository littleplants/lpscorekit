# LPSCoreKit

[![CI Status](http://img.shields.io/travis/Pituk Kaewsuksai/LPSCoreKit.svg?style=flat)](https://travis-ci.org/Pituk Kaewsuksai/LPSCoreKit)
[![Version](https://img.shields.io/cocoapods/v/LPSCoreKit.svg?style=flat)](http://cocoapods.org/pods/LPSCoreKit)
[![License](https://img.shields.io/cocoapods/l/LPSCoreKit.svg?style=flat)](http://cocoapods.org/pods/LPSCoreKit)
[![Platform](https://img.shields.io/cocoapods/p/LPSCoreKit.svg?style=flat)](http://cocoapods.org/pods/LPSCoreKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LPSCoreKit is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "LPSCoreKit"
```

## Author

Pituk Kaewsuksai, littleplantstudio@gmail.com

## License

LPSCoreKit is available under the MIT license. See the LICENSE file for more info.
